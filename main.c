#include <stdio.h>
#include "stdlib.h"
#include "time.h"
#include "stdbool.h"
#include "malloc.h"

int pole[4][4] = {0};
int score = 0;
char jmeno_hrace[] = {0};


typedef struct {
    int poradi;
    char *jmeno;
    int skore;
} HRAC;

typedef struct {
    int delka;
    HRAC **hraci;
} SEZNAM_HRACU;


void serad_hrace(SEZNAM_HRACU *seznamHracu) {
    HRAC *tmp = malloc(0);

    for (int i = 0; i < seznamHracu->delka; ++i) {
        for (int j = 0; j < seznamHracu->delka - i - 1; ++j) {
            if (seznamHracu->hraci[j]->skore < seznamHracu->hraci[j + 1]->skore) {
                tmp = seznamHracu->hraci[j];
                seznamHracu->hraci[j] = seznamHracu->hraci[j + 1];
                seznamHracu->hraci[j + 1] = tmp;
            }
        }
    }
   // free(tmp);

}

void menu_grafika() {
    printf("\n");
    for (int i = 0; i < 50; i++)printf(" ");
    printf(" ___   ___  _  _   ___  ");
    printf("\n");
    for (int i = 0; i < 50; i++)printf(" ");
    printf("|__ \\ / _ \\| || | / _ \\ ");
    printf("\n");
    for (int i = 0; i < 50; i++)printf(" ");
    printf("   ) | | | | || || (_) |");
    printf("\n");
    for (int i = 0; i < 50; i++)printf(" ");
    printf("  / /| | | |__   _> _ < ");
    printf("\n");
    for (int i = 0; i < 50; i++)printf(" ");
    printf(" / /_| |_| |  | || (_) |");
    printf("\n");
    for (int i = 0; i < 50; i++)printf(" ");
    printf("|____|\\___/   |_| \\___/ ");
}

void vypis_menu() {

    menu_grafika();

    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 50; ++j) {
            printf(" ");
        }
        if (i == 5) printf("|1. Nova hra          |\n");
        if (i == 6) printf("|2. Statistiky        |\n");
        printf("\n");
    }

    for (int k = 0; k < 50; k++)printf(" ");
    printf("ZH: ");
}

void vypis_hraciho_pole() {
    system("clear");

    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            printf("| %d | ", pole[i][j]);
        }
        printf("\n");
    }
    printf("\n Skore: %d\n", score);

}

bool je_souradnice_prazdna(int x, int y) {
    if (pole[x][y] != 0) return false;

    return true;
}

void vytvor_nove_souradnice(int *x, int *y) {
    do {
        *x = rand() % 4;
        *y = rand() % 4;
    } while (!je_souradnice_prazdna(*x, *y));
}

bool je_v_pole_prazdne_misto() {
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            if (pole[i][j] == 0) return true;
        }
    }

    return false;
}

void vlevo() {
    int i, j;
    for (i = 0; i < 4; i++) {
        for (j = 1; j < 4; j++) {
            int x = j, y = i;
            while (pole[y][x - 1] == 0 && x > 0) {
                pole[y][x - 1] = pole[y][x];
                pole[y][x--] = 0;
            }
        }
    }

    for (i = 0; i < 4; i++) {
        for (j = 0; j < 4; j++) {
            if (pole[i][j] == 0) continue;
            if (pole[i][j] == pole[i][j + 1] && j < 3) {
                score += pole[i][j] *= 2;
                pole[i][j + 1] = 0;
            }
        }
    }

    for (i = 0; i < 4; i++) {
        for (j = 1; j < 4; j++) {
            int x = j, y = i;
            while (pole[y][x - 1] == 0 && x > 0) {
                pole[y][x - 1] = pole[y][x];
                pole[y][x--] = 0;
            }
        }
    }

    // vypis_hraciho_pole();
}

void vpravo() {
    int i, j;
    for (i = 0; i < 4; i++) {
        for (j = 4 - 2; j >= 0; j--) {
            int x = j, y = i;
            while (pole[y][x + 1] == 0 && x < 4 - 1) {
                pole[y][x + 1] = pole[y][x];
                pole[y][x++] = 0;
            }
        }
    }

    for (i = 0; i < 4; i++) {
        for (j = 4 - 1; j >= 0; j--) {
            if (pole[i][j] == 0) continue;
            if (pole[i][j] == pole[i][j - 1] && j > 0) {
                score += pole[i][j] *= 2;
                pole[i][j - 1] = 0;
            }
        }
    }

    for (i = 0; i < 4; i++) {
        for (j = 4 - 2; j >= 0; j--) {
            int x = j, y = i;
            while (pole[y][x + 1] == 0 && x < 4 - 1) {
                pole[y][x + 1] = pole[y][x];
                pole[y][x++] = 0;
            }
        }
    }

}

void dolu() {
    int i, j;
    for (i = 4 - 2; i >= 0; i--) {
        for (j = 0; j < 4; j++) {
            int x = j, y = i;
            while (pole[y + 1][x] == 0 && y < 4 - 1) {
                pole[y + 1][x] = pole[y][x];
                pole[y++][x] = 0;
            }
        }
    }

    for (i = 4 - 1; i >= 0; i--) {
        for (j = 0; j < 4; j++) {
            if (pole[i][j] == 0) continue;
            if (pole[i][j] == pole[i - 1][j] && i > 0) {
                score += pole[i][j] *= 2;
                pole[i - 1][j] = 0;
            }
        }
    }

    for (i = 4 - 2; i >= 0; i--) {
        for (j = 0; j < 4; j++) {
            int x = j, y = i;
            while (pole[y + 1][x] == 0 && y < 4 - 1) {
                pole[y + 1][x] = pole[y][x];
                pole[y++][x] = 0;
            }
        }
    }
}

void nahoru() {
    int i, j;
    for (i = 1; i < 4; i++) {
        for (j = 0; j < 4; j++) {
            int x = j, y = i;
            while (pole[y - 1][x] == 0 && y > 0) {
                pole[y - 1][x] = pole[y][x];
                pole[y--][x] = 0;
            }
        }
    }

    for (i = 0; i < 4; i++) {
        for (j = 0; j < 4; j++) {
            if (pole[i][j] == 0) continue;
            if (pole[i][j] == pole[i + 1][j] && i < 4 - 1) {
                score += pole[i][j] *= 2;
                pole[i + 1][j] = 0;
            }
        }
    }

    for (i = 1; i < 4; i++) {
        for (j = 0; j < 4; j++) {
            int x = j, y = i;
            while (pole[y - 1][x] == 0 && y > 0) {
                pole[y - 1][x] = pole[y][x];
                pole[y--][x] = 0;
            }
        }
    }
}

bool je_vyhra() {
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            if (pole[i][j] == 2048) return true;
        }
    }

    return false;
}

bool je_mozny_pohyb() {
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            if ((pole[i][j] == pole[i][j + 1]) || (pole[i][j] == pole[i + 1][j])) return true;
        }
    }

    return false;
}

void statistika_zapis() {


    FILE *fp;
    fp = fopen("test.txt", "r");
    char c;
    int radek = 0, znak = 0;
    char *tmpStr = NULL;
    HRAC *hrac = malloc(0);
    SEZNAM_HRACU seznam;
    seznam.hraci = NULL;
    seznam.delka = 0;

    while (true) {
        c = fgetc(fp);
        if (c == '\n' || c == -1) {
            radek++;
            seznam.delka++;
            seznam.hraci = realloc(seznam.hraci, seznam.delka * sizeof(HRAC *));
            *(seznam.hraci + (seznam.delka - 1)) = hrac;
            znak = 0;
            hrac = malloc(0);
            if (c == -1) {
                free(hrac);
                break;
            };
        } else if (c == ' ') {
            hrac->jmeno = tmpStr;
            hrac->poradi = radek;

            tmpStr = NULL;

            znak = 0;
        } else if (c == ';') {
            hrac->skore = atoi(tmpStr);
            free(tmpStr);
            tmpStr = NULL;
        }
        else {
            tmpStr = realloc(tmpStr, (znak + 1) * sizeof(char));
            *(tmpStr + znak) = c;
            znak++;
        }
    }
    //free(hrac);
    hrac = malloc(0);
    hrac->jmeno = jmeno_hrace;
    hrac->skore = score;

    seznam.hraci = realloc(seznam.hraci, seznam.delka * sizeof(HRAC *));
    *(seznam.hraci + seznam.delka - 1) = hrac;

    fclose(fp);

    serad_hrace(&seznam);
    printf("\n\n\n\n");

    for (int i = 0; i < seznam.delka; ++i) {
        printf("Hrac %d: %s ,%d\n", i + 1, seznam.hraci[i]->jmeno, seznam.hraci[i]->skore);
    }
    fp = fopen("test.txt", "w");
    for (int i = 0; i < seznam.delka; ++i) {
        fprintf(fp, "%s %d;\n", seznam.hraci[i]->jmeno, seznam.hraci[i]->skore);
    }

    fclose(fp);
    for (int i = 0; i < seznam.delka - 1; ++i) {
        free(seznam.hraci[i]->jmeno);
        free(seznam.hraci[i]);
    }
    free(hrac);
    free(seznam.hraci);
}

void statistika_vypis() {
    FILE *fp;
    fp = fopen("test.txt", "r");
    char c;
    int radek = 0, znak = 0;
    char *tmpStr = NULL;
    HRAC *hrac = malloc(0);
    SEZNAM_HRACU seznam;
    seznam.hraci = NULL;
    seznam.delka = 0;

    while (true) {
        c = fgetc(fp);
        if (c == '\n' || c == -1) {
            radek++;
            seznam.delka++;
            seznam.hraci = realloc(seznam.hraci, seznam.delka * sizeof(HRAC *));
            *(seznam.hraci + (seznam.delka - 1)) = hrac;
            znak = 0;
            hrac = malloc(0);
            if (c == -1) {
                free(hrac);
                break;
            };
        } else if (c == ' ') {

            hrac->jmeno = tmpStr;
            hrac->poradi = radek;

            tmpStr = NULL;

            znak = 0;
        } else if (c == ';') {
            hrac->skore = atoi(tmpStr);
            free(tmpStr);
            tmpStr = NULL;
        }
        else {
            tmpStr = realloc(tmpStr, (znak + 1) * sizeof(char));
            *(tmpStr + znak) = c;
            znak++;
        }
    }

    printf("\n\n\n\n");
    for (int i = 0; i < seznam.delka; ++i) {
        printf("Hrac %d: %s ,%d\n", i + 1, seznam.hraci[i]->jmeno, seznam.hraci[i]->skore);
    }
    for (int i = 0; i < seznam.delka; ++i) {
        free(seznam.hraci[i]->jmeno);
        free(seznam.hraci[i]);
    }
    free(seznam.hraci);
    //  free(hrac);
}

void vysledek(bool vysledek) {
    system("clear");
    if (vysledek) printf("Vyhral jsi!");
    else printf("Prohral jsi!");

    getchar();
    statistika_zapis();

    exit(EOF);
}

void hraci_smycka() {
    int smer;
    int x, y;
    vypis_hraciho_pole();

    smer = getchar();
    if (smer == 50) dolu();
    else if (smer == 52) vlevo();
    else if (smer == 54) vpravo();
    else if (smer == 56) nahoru();
    else hraci_smycka();

    if (je_vyhra()) vysledek(true);

    if (je_v_pole_prazdne_misto()) {

        vytvor_nove_souradnice(&x, &y);
        pole[x][y] = 2;
    } else {
        if (!je_mozny_pohyb()) {
            vysledek(false);
        }
    }
    vypis_hraciho_pole();

    hraci_smycka();

}

void nova_hra() {
    int x1, y1, x2, y2;
    vytvor_nove_souradnice(&x1, &y1);

    vytvor_nove_souradnice(&x2, &y2);

    pole[x1][y1] = 2;
    pole[x2][y2] = 2;

    hraci_smycka();

}


int main() {

    srand(time(NULL));

    int vyber;

    vypis_menu();

    scanf("%d", &vyber);
    printf("Zadej svou hracskou prezdivku: ");
    scanf("%s", jmeno_hrace);

    if (vyber == 1) nova_hra();
    if (vyber == 2)statistika_vypis();
    else printf("Error");


    getchar();
    getchar();
    return 0;
}
